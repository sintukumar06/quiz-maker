package io.test.quiz;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class QuizMaker {
    protected int questionsInFile = 10;

    public void process(String filePath) {
        try {
            List<String> fileLines = Files.readAllLines(Paths.get(filePath));
            removeEmptyOrBlankLines(fileLines);
            printQuestionLists(getQuestionsFromFile(fileLines));
        } catch (Exception ex) {
            System.out.println("Exception occurred.");
        }
    }

    protected void removeEmptyOrBlankLines(List<String> fileLines) {
        // remove empty and blank lines from input question file
    }

    protected List<Question> getQuestionsFromFile(List<String> fileLines) {
        // Add logic to extract Question Object from file
        return null;
    }


    private void printQuestionLists(List<Question> questions) {
        questions.stream().forEach(q -> System.out.println(q.toString()));
    }
}
