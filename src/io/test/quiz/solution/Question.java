package io.test.quiz.solution;

import io.test.quiz.QuizMakerException;

import java.util.List;
import java.util.Objects;

public class Question {
    private String Question;

    public int getOptionCount() {
        return optionCount;
    }

    public void setOptionCount(int optionCount) {
        this.optionCount = optionCount;
    }

    private int optionCount;
    private List<String> options;

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        if (optionCount > 0 && answer > 0 && answer <= optionCount)
            this.answer = answer;
        else
            throw new QuizMakerException("Answer is not valid.");
    }

    private int answer;

    public String getQuestion() {
        return Question;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    @Override
    public String toString() {
        return "Question{" +
                "Question='" + Question + '\'' +
                ", optionCount=" + optionCount +
                ", options=" + options +
                ", answer=" + answer +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return optionCount == question.optionCount &&
                answer == question.answer &&
                Objects.equals(Question, question.Question) &&
                Objects.equals(options, question.options);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Question, optionCount, options, answer);
    }
}
