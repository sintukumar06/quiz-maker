package io.test.quiz.solution;

import io.test.quiz.QuizMakerException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class QuizMaker {
    public int questionsInFile = 10;
    private int currentLine = 0;

    public void process(String filePath) throws QuizMakerException {
        currentLine = 0;
        List<String> fileLines;
        try {
            fileLines = Files.readAllLines(Paths.get(filePath));
            removeEmptyOrBlankLines(fileLines);
            printQuestionLists(getQuestionsFromFile(fileLines));
        } catch (Exception ex) {
            System.out.println("Exception");
            throw new QuizMakerException(ex.getMessage());
        }
    }

    public void removeEmptyOrBlankLines(List<String> fileLines) {
        Iterator<String> itr = fileLines.iterator();
        while (itr.hasNext()) {
            String line = itr.next();
            if (line.trim().equalsIgnoreCase(""))
                itr.remove();
        }
    }

    private void printQuestionLists(List<Question> questions) {
        questions.stream().forEach(q -> System.out.println(q.toString()));
    }

    public List<Question> getQuestionsFromFile(List<String> fileLines) {
        List<Question> questionList = new ArrayList<>(questionsInFile);
        for (int i = 0; i < questionsInFile; i++) {
            Question question = new Question();
            question.setQuestion(getQuestion(fileLines));
            question.setOptionCount(getOptionCount(fileLines));
            question.setOptions(getQuestionOptions(fileLines));
            question.setAnswer(getAnswer(fileLines));
            questionList.add(question);
        }
        return questionList;
    }

    private String getQuestion(List<String> fileLines) {
        String question = "";
        while (!fileLines.get(currentLine).trim().matches("^\\d+$")) {
            question += fileLines.get(currentLine++);
        }
        return question;
    }

    private List<String> getQuestionOptions(List<String> fileLines) {
        List<String> options = new ArrayList<>();
        int optionCount = getOptionCount(fileLines);
        for (int i = 0; i < optionCount; i++) {
            options.add(fileLines.get(++currentLine));
        }
        if(!fileLines.get(++currentLine).trim().matches("^\\d+$"))
            throw new QuizMakerException("Question contains options more than expected options.");
        return options;
    }

    private Integer getOptionCount(List<String> fileLines) {
        return Integer.valueOf(fileLines.get(currentLine).trim());
    }

    private int getAnswer(List<String> fileLines) {
        try {
            return Integer.valueOf(fileLines.get(currentLine++).trim());
        } catch (NumberFormatException ex) {
            throw new QuizMakerException("Failed while parsing Answer at line : " + (currentLine));
        }
    }


}
