package io.test.quiz;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

// Uncomment below import to use class from solution package
//import io.test.quiz.solution.QuizMaker;
//import io.test.quiz.solution.Question;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class QuizMakerFunctionalTest {
    private QuizMaker testObject;

    @Before
    public void before() {
        testObject = new QuizMaker();
    }

    @Test(expected = QuizMakerException.class)
    public void invalidInputFilePathShouldResultInQuiZMakerException() throws IOException {
        String filePath = "data/file-not-exists.txt";
        testObject.process(filePath);
    }

    @Test
    public void emptyAndBlankLinesShouldBeIgnored() throws IOException {
        String filePath = "data/input-file-with-empty-blank-lines.txt";
        List<String> fileLines = Files.readAllLines(Paths.get(filePath));
        testObject.removeEmptyOrBlankLines(fileLines);

        assertThat(fileLines, notNullValue());
        assertThat(fileLines.size(), equalTo(6));
    }

    @Test
    public void readInputFileContainingOneQuestionOnly() throws IOException {
        testObject.questionsInFile = 1;
        String filePath = "data/input-file-with-one-question.txt";
        List<String> fileLines = Files.readAllLines(Paths.get(filePath));
        List<Question> questions = testObject.getQuestionsFromFile(fileLines);

        assertThat(questions, notNullValue());
        assertThat(questions.size(), is(not(0)));
        assertThat(questions.get(0).getQuestion(), equalTo("What is 5+5=?"));
        assertThat(questions.get(0).getAnswer(), is(1));
        assertThat(questions.get(0).getOptions().size(), equalTo(3));
        assertThat(questions.get(0).getOptions(), hasItems(
                "1) 10",
                "2) 15",
                "3) 20"
        ));
    }

    @Test
    public void readInputFileContainingTwoQuestionOnly() throws IOException {
        testObject.questionsInFile = 2;
        String filePath = "data/input-file-with-two-question.txt";
        List<String> fileLines = Files.readAllLines(Paths.get(filePath));
        List<Question> questions = testObject.getQuestionsFromFile(fileLines);

        assertThat(questions, notNullValue());
        assertThat(questions.size(), is(2));
        assertThat(questions.get(0).getQuestion(), equalTo("What is 5+5=?"));
        assertThat(questions.get(0).getAnswer(), is(1));
        assertThat(questions.get(0).getOptions().size(), equalTo(3));
        assertThat(questions.get(1).getOptions().size(), equalTo(5));
    }

/*    @Test
    public void readInputFileContainingOneQuestionWithInvalidAnswerText() throws IOException {
        testObject.questionsInFile = 1;
        String filePath = "data/input-file-with-one-question-having-invalid-answer.txt";
        try {
            List<String> fileLines = Files.readAllLines(Paths.get(filePath));
            testObject.getQuestionsFromFile(fileLines);
        } catch (QuizMakerException ex) {
            assertThat(ex, instanceOf(QuizMakerException.class));
            assertThat(ex.getMessage(), equalTo("Failed while parsing Answer at line : 6"));
        }
    }*/

    @Test
    public void readInputFileContainingOneQuestionWithAnswerNotInOptions() throws IOException {
        testObject.questionsInFile = 1;
        String filePath = "data/input-file-with-one-question-with-answer-not-in-option.txt";
        try {
            List<String> fileLines = Files.readAllLines(Paths.get(filePath));
            testObject.getQuestionsFromFile(fileLines);
            Assert.fail();
        } catch (QuizMakerException ex) {
            assertThat(ex, instanceOf(QuizMakerException.class));
            assertThat(ex.getMessage(), equalTo("Answer is not valid."));
        }
    }

    @Test
    public void readInputFileContainingQuestionWithMoreThanExpectedOptions() throws IOException {
        testObject.questionsInFile = 1;
        String filePath = "data/input-file-with-two-question-with-more-than-expected-option.txt";
        try {
            List<String> fileLines = Files.readAllLines(Paths.get(filePath));
            testObject.getQuestionsFromFile(fileLines);
            Assert.fail();
        } catch (QuizMakerException ex) {
            assertThat(ex, instanceOf(QuizMakerException.class));
            assertThat(ex.getMessage(), equalTo("Question contains options more than expected options."));
        }
    }
}