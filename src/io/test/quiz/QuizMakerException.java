package io.test.quiz;

public class QuizMakerException extends RuntimeException {
    public QuizMakerException(String msg) {
        super(msg);
    }
}
