package io.test.quiz;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

// Uncomment below import to use class from solution package
//import io.test.quiz.solution.QuizMaker;
//import io.test.quiz.solution.Question;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class QuizMakerE2ETests {
    private QuizMaker testObject;

    @Before
    public void before() {
        testObject = new QuizMaker();
    }

    @Test(expected = QuizMakerException.class)
    public void invalidInputFilePathShouldResultInQuiZMakerException() throws IOException {
        String filePath = "data/file-not-exists.txt";
        testObject.process(filePath);
    }

    @Test(expected = QuizMakerException.class)
    public void ProcessingFileWithLessThanTenQuestionShouldResultInException() throws IOException {
        String filePath = "data/input-file-with-two-question.txt";
        testObject.process(filePath);
    }

    @Test
    public void readInputFileContainingTenQuestions() throws IOException {
        String filePath = "data/input-file-with-ten-question.txt";
        List<String> fileLines = Files.readAllLines(Paths.get(filePath));
        List<Question> questions = testObject.getQuestionsFromFile(fileLines);

        assertThat(questions, notNullValue());
        assertThat(questions.size(), is(10));
        assertThat(questions.get(0).getQuestion(), equalTo("1> What is 5+5=?"));
        assertThat(questions.get(0).getAnswer(), is(1));
        assertThat(questions.get(0).getOptions().size(), equalTo(3));
        assertThat(questions.get(0).getOptions(), hasItems(
                "1) 10",
                "2) 15",
                "3) 20"
        ));

        assertThat(questions.get(9).getQuestion(), equalTo("10> What is 5+5=?"));
        assertThat(questions.get(9).getAnswer(), is(4));
        assertThat(questions.get(9).getOptions().size(), equalTo(5));
        assertThat(questions.get(9).getOptions(), hasItems(
                "1) 10",
                "2) 15",
                "3) 20",
                "4) 30",
                "5) 05"
        ));
    }
}
